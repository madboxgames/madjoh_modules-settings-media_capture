define([
	'require',
	'modules/media_selector'
],
function(require, MediaSelector){
	var MediaCapture = {
		page : null,

		init : function(controller){
			if(MediaCapture.page) return;
			MediaCapture.page = new MediaSelector();
			MediaCapture.page.build(controller);
		},

		show : function(options){
			MediaCapture.page.prepare(options);
			MediaCapture.page.show();
		},
		hide : function(){
			MediaCapture.page.hide();
		}
	};

	return MediaCapture;
});